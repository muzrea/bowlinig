package com.twuc.app;

public class BowlingFrame {
    public static final int NO_THROW_NUMBER = -1;
    private int firstNumber = NO_THROW_NUMBER;
    private int secondNumber = NO_THROW_NUMBER;

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int positiveCount() {
        return (firstNumber == NO_THROW_NUMBER ? 0 : firstNumber)  +
                (secondNumber == NO_THROW_NUMBER ? 0 : secondNumber);
    }

    public boolean isSpareBonus() {
        return this.firstNumber > 0 && this.firstNumber < 10 && this.positiveCount() == 10;
    }

    public boolean isStrikeBonus() {
        return firstNumber == 10;
    }
}
