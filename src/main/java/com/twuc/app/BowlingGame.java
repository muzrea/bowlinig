package com.twuc.app;

import java.util.ArrayList;
import java.util.List;

import static com.twuc.app.BowlingFrame.NO_THROW_NUMBER;

public class BowlingGame {
    private List<BowlingFrame> bowlingFrames;
    private BowlingFrame currentFrame;

    public BowlingGame() {
        this.currentFrame = new BowlingFrame();
        this.bowlingFrames = new ArrayList<>();
        this.bowlingFrames.add(currentFrame);
    }

    public int getTotalScore() {
        int result = 0;
        for (int i = 0; i < bowlingFrames.size(); i++) {
            BowlingFrame currentFrame = bowlingFrames.get(i);
            result += currentFrame.positiveCount();
            if (i > 0 && i < 10) {
                BowlingFrame lastFrame = bowlingFrames.get(i - 1);
                if (lastFrame.isSpareBonus()) {
                    result += currentFrame.getFirstNumber();
                }
                if (lastFrame.isStrikeBonus()) {
                    result += currentFrame.positiveCount();
                    if (i > 1) {
                        BowlingFrame lastBeforeFrame = bowlingFrames.get(i - 2);
                        if (lastBeforeFrame.isStrikeBonus()) {
                            result += currentFrame.positiveCount();
                        }
                    }
                }
            } else if (i == 10) {
                if(currentFrame.isStrikeBonus()){
                    result += 10;
                }
            }
        }
        return result;
    }

    public void throwBall(int pinNumber) {
        if (currentFrame.getFirstNumber() == NO_THROW_NUMBER) {
            currentFrame.setFirstNumber(pinNumber);
        } else {
            currentFrame.setSecondNumber(pinNumber);
            BowlingFrame bowlingFrame = new BowlingFrame();
            currentFrame = bowlingFrame;
            bowlingFrames.add(currentFrame);
        }
    }
}
