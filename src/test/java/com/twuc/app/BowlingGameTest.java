package com.twuc.app;

import org.junit.Test;

import static java.util.Arrays.*;
import static org.junit.Assert.assertEquals;


public class BowlingGameTest {
    @Test
    public void should_return_0_when_no_throws() {
        BowlingGame bowlingGame = new BowlingGame();
        assertEquals(0, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_number_of_pins_knocked_down_when_one_throw() {
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.throwBall(8);
        assertEquals(8, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_number_of_pins_knocked_down_when_two_throws() {
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.throwBall(8);
        bowlingGame.throwBall(8);
        assertEquals(16, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(2,3,4,3,2,4,7,8,2,3,4,3,2,4,7,8,4,3,2,4)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(79, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_first_spare_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(7,3,4,3,2,4,1,8,2,3,4,3,2,4,7,1,4,3,2,4)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(71 + 4, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_spare_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(7,3,4,6,2,4,2,8,2,3,4,3,2,4,7,1,4,3,2,4)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(75+8, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_last_spare_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(7,3,4,6,2,4,2,8,2,3,4,3,2,4,7,1,4,3,6,4,3)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(75+12+3, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_first_strike_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(10,0,4,6,2,4,2,8,2,3,4,3,2,4,7,1,4,3,6,3)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(78 + 10 + 2 + 2, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_more_strike_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(10,0,4,5,10,0,2,7,2,3,4,3,2,4,7,1,4,3,6,3)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(80+9+9, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_last_strike_bonus() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(10,0,4,5,10,0,2,7,2,3,4,3,2,4,7,1,4,3,10,0,2,6)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(81+9+9+8, bowlingGame.getTotalScore());
    }

    @Test
    public void should_return_total_score_when_ten_frames_with_ten_strikes() {
        BowlingGame bowlingGame = new BowlingGame();
        asList(10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,10)
                .stream()
                .forEach(pinNumber -> {
                    bowlingGame.throwBall(pinNumber);});
        assertEquals(300, bowlingGame.getTotalScore());
    }

}
